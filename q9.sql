select category_name, sum(item_price) as total_price
from item_category a
join item b
on a.category_id = b.category_id
group by a.category_name
order by total_price desc;
